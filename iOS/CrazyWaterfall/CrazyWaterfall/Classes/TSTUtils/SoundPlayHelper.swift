//
//  SoundPlayHelper.m
//  CocosSwift
//
//  Created by Thales Toniolo on 10/09/14.
//  Copyright (c) 2014 Flameworks. All rights reserved.
//
import Foundation

enum GameMusicAndSoundFx:String {
	case MusicInGame = "blues.mp3"
    case MusicHomeScene = "background.mp3"
    case MusicPausedGame = "waterfall1.mp3"
	case SoundFXButtonTap = "SoundFXButtonTap.mp3"
    case SoundFXGameOver = "game_over.mp3"
    case SoundFXCrash = "colision.mp3"
    case SoundFXLaugh = "laugh.mp3"
	
	static let allSoundFx = [SoundFXButtonTap, SoundFXGameOver, SoundFXLaugh, SoundFXCrash]
}

class SoundPlayHelper {
	// MARK Public Declarations
	
	// MARK Private Declarations

    private var bgMusicStt:Bool = true
    private var fxSoundsStt:Bool = true
    
	// MARK: - Singleton
	class var sharedInstance:SoundPlayHelper {
	struct Static {
			static var instance: SoundPlayHelper?
			static var token: dispatch_once_t = 0
		}
		
		dispatch_once(&Static.token) {
			Static.instance = SoundPlayHelper()
		}

		return Static.instance!
	}
	
	// MARK: Private Methods

	// MARK: Public Methods
	func preloadSoundsAndMusic() {
		// Habilita o cache de audio
		OALSimpleAudio.sharedInstance().preloadCacheEnabled = true

		// Apenas uma musica de fundo pode ser cacheada
		OALSimpleAudio.sharedInstance().preloadBg(GameMusicAndSoundFx.MusicHomeScene.rawValue)

		// Itera todos os SoundsFX para cachear
		for music in GameMusicAndSoundFx.allSoundFx {
			OALSimpleAudio.sharedInstance().preloadEffect(music.rawValue)
		}

		// Define o volume default
		setMusicDefaultVolume()
	}

	func playSoundWithControl(aGameMusic:GameMusicAndSoundFx) {
        if (self.fxSoundsStt){
            OALSimpleAudio.sharedInstance().playEffect(aGameMusic.rawValue)
        }
	}

	func playMusicWithControl(aGameMusic:GameMusicAndSoundFx, withLoop:Bool) {
        if (self.bgMusicStt){ //LAP - So toca se status ON
            OALSimpleAudio.sharedInstance().stopBg()
            OALSimpleAudio.sharedInstance().preloadBg(aGameMusic.rawValue)
            OALSimpleAudio.sharedInstance().playBgWithLoop(withLoop)
        }
	}

	func stopAllSounds() {
		OALSimpleAudio.sharedInstance().stopEverything()
	}

	func setMusicVolume(aVolume:Float) {
		OALSimpleAudio.sharedInstance().bgVolume = aVolume
	}

	func setMusicPauseVolume() {
		OALSimpleAudio.sharedInstance().bgVolume = 1.0
	}

	func setMusicDefaultVolume() {
		OALSimpleAudio.sharedInstance().bgVolume = 0.8
		OALSimpleAudio.sharedInstance().effectsVolume = 1.0
	}
    
    func setStatusBgMusic(){
        self.bgMusicStt = (self.bgMusicStt == true ? false : true)
    }
    
    func setStatusFxSounds(){
        self.fxSoundsStt = (self.fxSoundsStt == true ? false : true)
    }
    
    func getStatusBgMusic() -> Bool{
        return self.bgMusicStt
    }
    
    func getStatusFxSounds() -> Bool{
        return self.fxSoundsStt
    }
}
