//
//  PropertyHelper.swift
//  CrazyWaterfall
//
//  Created by Leandro Pereira on 05/04/15.
//  Copyright (c) 2015 Flameworks. All rights reserved.
//

import Foundation

class PropertyHelper {
    	// MARK: - Singleton
    private let path = NSBundle.mainBundle().pathForResource("CrazyProperty", ofType: "plist")
    
	class var sharedInstance:PropertyHelper {
	struct Static {
            static var instance: PropertyHelper?
            static var token: dispatch_once_t = 0
		}

		dispatch_once(&Static.token) {
			Static.instance = PropertyHelper()
		}
		return Static.instance!
	}
    
    func getHiScore() -> Int{
        var score:Int = 0
        
        if (NSUserDefaults.standardUserDefaults().objectForKey("hiScore") != nil) {
            score = (NSUserDefaults.standardUserDefaults().objectForKey("hiScore") as! NSString).integerValue
        }
        
        return score
    }
    
    func setHiScore(score:Int){
        if (score > self.getHiScore()){
            let strScore:String = "\(score)"
        
            NSUserDefaults.standardUserDefaults().setValue(strScore, forKey: "hiScore")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}