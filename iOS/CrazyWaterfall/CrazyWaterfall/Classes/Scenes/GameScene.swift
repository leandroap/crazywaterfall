//
//  GameScene.swift
//  CocosSwift
//
//  Created by Usuário Convidado on 18/03/15.
//  Copyright (c) 2014 Flameworks. All rights reserved.
//
import Foundation
import UIKit
import CoreMotion

// MARK: - Class Definition
class GameScene: CCScene{
	// MARK: - Public Objects
    var score:Int = 0
	
	// MARK: - Private Objects
	private let screenSize:CGSize = CCDirector.sharedDirector().viewSize()
    private var physicsWorld:CCPhysicsNode = CCPhysicsNode()
    private var picaPau:PicaPau = PicaPau(imageNamed:"barrel.png")
    private var pedra:Pedra = Pedra(imageNamed:"stone1.png")
    private var pedraSpeed:CCTime = 10.0 //CCTime(arc4random_uniform(6)) + 5.0 // De 5s a 10s
    private var canPlay:Bool = true
    private var isPaused:Bool = false
	private var isTouching:Bool = false
    private var arrPedras:[CCSprite] = []
    private var conteudoScore:CCLabelTTF = CCLabelTTF(string: "0", fontName: "Noteworthy-Bold", fontSize: 36.0)
    
    private let bg10:CCSprite = CCSprite(imageNamed: "bg1.png")
    private let bg20:CCSprite = CCSprite(imageNamed: "bg2.png")
    private let bg30:CCSprite = CCSprite(imageNamed: "bg3.png")
    
    private let bg11:CCSprite = CCSprite(imageNamed: "bg1.png")
    private let bg21:CCSprite = CCSprite(imageNamed: "bg2.png")
    private let bg31:CCSprite = CCSprite(imageNamed: "bg3.png")
    
    private let resumeButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("resume.png").spriteFrame)
    private let replayButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("replay.png").spriteFrame)
    private let backButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("exit.png").spriteFrame)
    
    private var piso:CCNode = CCNode()
    private var teto:CCNode = CCNode()
    private var bordaD:CCNode = CCNode()
    private var bordaE:CCNode = CCNode()
    
    private var parallaxNode1:CCParallaxNode = CCParallaxNode()
    private var parallaxNode2:CCParallaxNode = CCParallaxNode()
    private var parallaxNode3:CCParallaxNode = CCParallaxNode()
    
    private var motionManager: CMMotionManager = CMMotionManager()
    private let picaPauSpeed:CGFloat = 75
    private var picaPauDestX:CGFloat = 0.0
    private var picaPauDesty:CGFloat = 0.0
    
	// MARK: - Life Cycle
	override init() {
		super.init()
        
        // Configura os objetos na tela
        self.createSceneObjects()
        self.setUp_bordas()
        
        //LAP - Controle para inverter o pica pau
        var flipX:Bool = false
        
        if (self.motionManager.accelerometerAvailable) {
            self.motionManager.deviceMotionUpdateInterval = 1/30
            self.motionManager.startDeviceMotionUpdatesToQueue(NSOperationQueue.currentQueue(),
                withHandler:{ deviceManager, error in
                    
                    let currentX = self.picaPau.position.x
                    //let limiteD = self.screenSize.width - 5
                    let acceleration: CMAcceleration = self.motionManager.accelerometerData.acceleration
                    
                    if (acceleration.x < 0) { // tilting the device to the right
                        self.picaPauDestX = currentX + CGFloat(acceleration.x) * self.picaPauSpeed
                        flipX = true
                    } else if (acceleration.x > 0) { // tilting the device to the left
                        self.picaPauDestX = currentX + CGFloat(acceleration.x) * self.picaPauSpeed
                        flipX = false
                    }
//                    if( self.picaPauDestX > limiteD){
//                       self.picaPauDestX = currentX
//                    }
                    self.movePicaPau(flipX)
            })
        } else {
            // Libera o uso de toque na tela
            self.userInteractionEnabled = true
        }
	}

	override func onEnter() {
		// Chamado apos o init quando entra no director
		super.onEnter()
        
        self.motionManager.startAccelerometerUpdates()
        self.motionManager.accelerometerActive == true
        
        // Inicia a geracao de inimigos apos 3s de inicio de jogo
        DelayHelper.sharedInstance.callFunc("createPedras", onTarget: self, withDelay: 3.0)
	}

	// Tick baseado no FPS
	override func update(delta: CCTime) {
        if (self.canPlay || self.isPaused) {
            //LAP - Controle do Paralax
            self.updateBackground(delta)
        }
        
        if (self.canPlay) {
            // Valida colisao entre o player e as picos
            for pedra:CCSprite in self.arrPedras {
                if (self.picaPau.canColide) {
                    if (CGRectIntersectsRect(self.picaPau.boundingBox(), pedra.boundingBox())) {
                        self.gameOver()
                    }
                }
            }
        }
	}
    
    func gameOver(){
        self.canPlay = false

        SoundPlayHelper.sharedInstance.playSoundWithControl(GameMusicAndSoundFx.SoundFXCrash)
        SoundPlayHelper.sharedInstance.playSoundWithControl(GameMusicAndSoundFx.SoundFXGameOver)
        
        PropertyHelper.sharedInstance.setHiScore(self.score)
        
        //LAP - Para o Acelerometro
        self.motionManager.stopAccelerometerUpdates()
        self.motionManager.accelerometerActive == false
        
        let imgGameOver:CCSprite = CCSprite(imageNamed: "game_over.png")
        imgGameOver.position = CGPointMake(self.screenSize.width/2, self.screenSize.height/1.7)
        imgGameOver.anchorPoint = CGPointMake(0.5, 0.5)
        imgGameOver.scale = 0.6
        
        self.physicsWorld.addChild(imgGameOver, z:10)
        
        let replayButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("replay.png").spriteFrame)
        replayButton.position = CGPointMake(imgGameOver.position.x , imgGameOver.position.y - 200)
        replayButton.anchorPoint = CGPointMake(0.5, 0.5)
        replayButton.zoomWhenHighlighted = false
        replayButton.scale = 0.3
        replayButton.block = {_ in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.GameScene, isFade:true)
        }
        
        self.physicsWorld.addChild(replayButton, z:10)
        
        // Back button
        let backButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("exit.png").spriteFrame)
        backButton.position = CGPointMake(replayButton.position.x , replayButton.position.y - 150)
        backButton.anchorPoint = CGPointMake(0.5, 0.5)
        backButton.zoomWhenHighlighted = false
        backButton.scale = 0.3
        backButton.block = {_ in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.HomeScene, isFade:true)
        }

        self.physicsWorld.addChild(backButton, z:10)
    }
    
    func gamePause(){
        if(self.isPaused){
            return
        }
        
        self.isPaused = true
        self.canPlay = false
        PropertyHelper.sharedInstance.setHiScore(self.score)
        
        SoundPlayHelper.sharedInstance.setMusicPauseVolume()
        SoundPlayHelper.sharedInstance.playMusicWithControl(GameMusicAndSoundFx.MusicPausedGame, withLoop: true)
        
        //LAP - Para o Acelerometro
        self.motionManager.stopAccelerometerUpdates()
        self.motionManager.accelerometerActive == false
        
        self.resumeButton.position = CGPointMake(self.screenSize.width/2, self.screenSize.height/1.7)
        self.resumeButton.anchorPoint = CGPointMake(0.5, 0.5)
        self.resumeButton.zoomWhenHighlighted = false
        self.resumeButton.scale = 0.6
        self.resumeButton.block = {_ in
            self.gameResume()
        }
        
        self.physicsWorld.addChild(resumeButton, z:10)
        
        
        self.replayButton.position = CGPointMake(resumeButton.position.x , resumeButton.position.y - 200)
        self.replayButton.anchorPoint = CGPointMake(0.5, 0.5)
        self.replayButton.zoomWhenHighlighted = false
        self.replayButton.scale = 0.3
        self.replayButton.block = {_ in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.GameScene, isFade:true)
        }
        
        self.physicsWorld.addChild(replayButton, z:10)
        
        // Back button
        
        self.backButton.position = CGPointMake(replayButton.position.x , replayButton.position.y - 150)
        self.backButton.anchorPoint = CGPointMake(0.5, 0.5)
        self.backButton.zoomWhenHighlighted = false
        self.backButton.scale = 0.3
        self.backButton.block = {_ in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.HomeScene, isFade:true)
        }
        
        self.physicsWorld.addChild(backButton, z:10)
    }
    
    func gameResume(){
        self.canPlay = true
        self.isPaused = false
        
        self.resumeButton.removeFromParentAndCleanup(true)
        self.replayButton.removeFromParentAndCleanup(true)
        self.backButton.removeFromParentAndCleanup(true)
        
        self.motionManager.startAccelerometerUpdates()
        self.motionManager.accelerometerActive == true
        
        SoundPlayHelper.sharedInstance.setMusicDefaultVolume()
        SoundPlayHelper.sharedInstance.playMusicWithControl(GameMusicAndSoundFx.MusicInGame, withLoop: true)
        
        self.createPedras()
    }

	// MARK: - Private Methods
    func createSceneObjects() {
        self.makeLabels()

        // Define o mundo
        self.physicsWorld.collisionDelegate = self
        self.physicsWorld.gravity = CGPointZero
        self.addChild(self.physicsWorld, z:0)

        // Configura o parallax infinito
        self.bg10.position = CGPointMake(0.0, 0.0)
        self.bg10.anchorPoint = CGPointMake(0.0, 0.0)
        self.bg11.position = CGPointMake(0.0, 0.0)
        self.bg11.anchorPoint = CGPointMake(0.0, 0.0)

        self.bg20.position = CGPointMake(0.0, 0.0)
        self.bg20.anchorPoint = CGPointMake(0.0, 0.0)
        self.bg21.position = CGPointMake(0.0, 0.0)
        self.bg21.anchorPoint = CGPointMake(0.0, 0.0)
        
        self.bg30.position = CGPointMake(0, 0)
        self.bg30.anchorPoint = CGPointMake(0, 0)
        self.bg31.position = CGPointMake(0, 0)
        self.bg31.anchorPoint = CGPointMake(0, 0)

        self.bg30.scale = 0.5
        self.bg31.scale = 0.5

        // LAP - Fundo Azul
        self.parallaxNode1.position = CGPointMake(0.0, 0.0)
        self.parallaxNode1.addChild(self.bg10, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, 0.0))
        self.parallaxNode1.addChild(self.bg11, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, self.bg10.contentSize.height - 5.0))
        
        self.physicsWorld.addChild(self.parallaxNode1, z:0)

        // LAP - Fundo branco
        self.parallaxNode2.position = CGPointMake(0.0, 0.0)
        self.parallaxNode2.addChild(self.bg20, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, 0.0))
        self.parallaxNode2.addChild(self.bg21, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, self.bg20.contentSize.height))
        
        self.physicsWorld.addChild(self.parallaxNode2, z:0)

        // LAP - Fundo Paredes
        self.parallaxNode3.position = CGPointMake(0.0, 0.0)
        self.parallaxNode3.addChild(self.bg30, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, 0.0))
        self.parallaxNode3.addChild(self.bg31, z: 1, parallaxRatio:CGPointMake(0.0, 1.0),
            positionOffset:CGPointMake(0.0, -self.bg30.contentSize.height))
        
        self.physicsWorld.addChild(self.parallaxNode3, z:0)

        // Configura o heroi na tela
        self.picaPauDestX = screenSize.width/2.0
        self.picaPauDesty = screenSize.height/1.5
        self.picaPau.position = CGPointMake(screenSize.width/2.0, screenSize.height)
        self.picaPau.scale = 1.0
        self.physicsWorld.addChild(self.picaPau, z:ObjectsLayers.Player.rawValue)
        
        let scaleTo = CCActionScaleTo(duration: 0.95, scale: 0.5)
        let moveBy = CCActionMoveBy(duration: 1, position: CGPointMake(0, -350))
        self.picaPau.runAction(CCActionSpawn(one: scaleTo, two: moveBy))
        
    }
    
    func makeLabels(){
        
        let topPoint:CGFloat = (self.screenSize.height / 1.1) + 50
        
        //LAP - Pause Button
        var pauseButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("pause.png").spriteFrame)
        pauseButton.block = {(sender:AnyObject!) -> Void in
            self.gamePause()
            
        }
        pauseButton.position = CGPointMake((self.screenSize.width - self.screenSize.width) + 50, topPoint)
        pauseButton.anchorPoint = CGPointMake(0.5, 0.5)
        pauseButton.scale = 0.2
        self.addChild(pauseButton, z:1)
        
        //LAP - Score Label
        let imgScore:CCSprite = CCSprite(imageNamed: "score.png")
        imgScore.position = CGPointMake(self.screenSize.width/2, topPoint)
        imgScore.anchorPoint = CGPointMake(0.5, 0.5)
        imgScore.scale = 0.2
        self.addChild(imgScore, z:1)
        
        self.conteudoScore.color = CCColor.redColor()
        self.conteudoScore.position = CGPointMake(imgScore.position.x + imgScore.boundingBox().width + 20, topPoint)
        self.conteudoScore.anchorPoint = CGPointMake(0.5, 0.5)
        self.addChild(conteudoScore, z:1)
    }

    func createPedras() {
        if (!self.canPlay) {
            return
        }
        
        let pedra:Pedra = Pedra()//Pedra(imageNamed:"stone1.png")
        pedra.gameSceneRef = self // Envia propria referencia para controlar os disparos que ficaram nesta classe
        pedra.anchorPoint = CGPointMake(0.5, 0.5)
        pedra.scale = 0.5
        let minScreenX:CGFloat = pedra.boundingBox().size.width
        let maxScreenX:UInt32 = UInt32(screenSize.width - (pedra.boundingBox().size.width + minScreenX))
        var xPosition:CGFloat = minScreenX + CGFloat(arc4random_uniform(maxScreenX))
        pedra.position = CGPointMake(xPosition, -pedra.boundingBox().size.height)

        self.physicsWorld.addChild(pedra, z:ObjectsLayers.Player.rawValue)
        
        arrPedras.append(pedra);
        
        pedra.runAction(CCActionSequence.actionOne(CCActionMoveTo.actionWithDuration(
            self.pedraSpeed,
            position:CGPointMake(pedra.position.x,
            pedra.boundingBox().size.height + screenSize.height))
            as! CCActionFiniteTime, two: CCActionCallBlock.actionWithBlock(
                {
                    _ in pedra.removeFromParentAndCleanup(true)
                    
                    //Pontua sempre que a pedra sai da cena
                    if (self.canPlay){
                        self.score += 10
                        self.conteudoScore.string = "\(self.score)"
                        //String(format: "%05f", self.score)
                    }
                }
            ) as! CCActionFiniteTime) as! CCAction)
        
        var delay:CCTime = (CCTime(arc4random_uniform(101)) / 100.0) + 0.5 // De 0.5s a 1.5s
        DelayHelper.sharedInstance.callFunc("createPedras", onTarget: self, withDelay: delay)
    }
    
    func updateBackground(delta: CCTime){
        // ==================== CONTROLE DO PARALLAX
        //LAP - Fundo
        var backgroundScrollVel:CGPoint = CGPointMake(0, -400)
        
        // Soma os pontos (posicao atual + (velocidade * delta))
        let pt1:CGFloat = backgroundScrollVel.y * CGFloat(delta)
        let multiDelta:CGPoint = CGPointMake(backgroundScrollVel.x, pt1)
        self.parallaxNode1.position = CGPointMake(0.0, self.parallaxNode1.position.y + multiDelta.y)
        self.parallaxNode2.position = CGPointMake(0.0, self.parallaxNode2.position.y + multiDelta.y)
        
        //LAP - Paredes
        var paredesScrollVel:CGPoint = CGPointMake(0, -100)
        
        // Soma os pontos (posicao atual + (velocidade * delta))
        let pt2:CGFloat = paredesScrollVel.y * CGFloat(delta)
        let multiDelta2:CGPoint = CGPointMake(paredesScrollVel.x, pt2)
        self.parallaxNode3.position = CGPointMake(0.0, self.parallaxNode3.position.y - multiDelta2.y)
        
        // Valida quando a imagem chega ao fim para reposicionar as imagens
        if (self.parallaxNode1.position.y < -self.bg10.contentSize.height) {
            self.parallaxNode1.position = CGPointMake(0.0, 0.0)
        }
        
        if (self.parallaxNode2.position.y < -self.bg20.contentSize.height) {
            self.parallaxNode2.position = CGPointMake(0.0, 0.0)
        }
        
        if (self.parallaxNode3.position.y > self.bg30.contentSize.height) {
            self.parallaxNode3.position = CGPointMake(0.0, 0.0)
        }
    }
    
	// MARK: - Public Methods
    func setUp_bordas(){
        //CGRectMake(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat)
        
        let maxX:CGFloat = self.screenSize.width
        let minX:CGFloat = self.screenSize.width - self.screenSize.width
        let maxY:CGFloat = self.screenSize.height
        let minY:CGFloat = self.screenSize.height - self.screenSize.height
        let sizeX:CGFloat = self.screenSize.width
        let sizeY:CGFloat = self.screenSize.height
        
        piso.physicsBody = CCPhysicsBody(rect: CGRectMake(minX, minY, sizeY, 5), cornerRadius: 0.0)
        piso.physicsBody.type = CCPhysicsBodyType.Kinematic
        piso.physicsBody.friction = 1.0
        piso.physicsBody.elasticity = 0.5
        piso.physicsBody.collisionCategories = ["borda"]
        piso.physicsBody.collisionMask = ["picapau"]
        self.physicsWorld.addChild(piso, z:ObjectsLayers.Player.rawValue)
        
        
        teto.physicsBody = CCPhysicsBody(rect: CGRectMake(maxX, minY, sizeY, -5), cornerRadius: 0.0)
        teto.physicsBody.type = CCPhysicsBodyType.Kinematic
        teto.physicsBody.friction = 1.0
        teto.physicsBody.elasticity = 0.5
        teto.physicsBody.collisionCategories = ["borda"]
        teto.physicsBody.collisionMask = ["picapau"]
        self.physicsWorld.addChild(teto, z:ObjectsLayers.Player.rawValue)
        
        bordaD.physicsBody = CCPhysicsBody(rect: CGRectMake(minX, maxY, sizeX, -5), cornerRadius: 0.0)
        bordaD.physicsBody.type = CCPhysicsBodyType.Kinematic
        bordaD.physicsBody.friction = 1.0
        bordaD.physicsBody.elasticity = 0.5
        bordaD.physicsBody.collisionCategories = ["borda"]
        bordaD.physicsBody.collisionMask = ["picapau"]
        self.physicsWorld.addChild(bordaD, z:ObjectsLayers.Player.rawValue)
        
        bordaE.physicsBody = CCPhysicsBody(rect: CGRectMake(minX, minY, 5, sizeX), cornerRadius: 0.0)
        bordaE.physicsBody.type = CCPhysicsBodyType.Kinematic
        bordaE.physicsBody.friction = 1.0
        bordaE.physicsBody.elasticity = 0.5
        bordaE.physicsBody.collisionCategories = ["borda"]
        bordaE.physicsBody.collisionMask = ["picapau"]
        self.physicsWorld.addChild(bordaE, z:ObjectsLayers.Player.rawValue)
    }
    
    func movePicaPau(flipX:Bool){
        if (!self.canPlay) {
            return
        }
        
        let destY = CGFloat(self.picaPau.position.y)
        let action = CCActionMoveTo(duration: 0.5, position: CGPointMake(self.picaPauDestX, destY))
        let actionFlippedX = CCActionFlipX(flipX: flipX)
        self.picaPau.runAction(actionFlippedX)
        self.picaPau.runAction(action)
    }
    
	// MARK: - Delegates/Datasources
    
    // MARK: - Touchs Delegates
    override func touchBegan(touch: UITouch!, withEvent event: UIEvent!) {
        if (self.canPlay) {
            
            self.isTouching = true
            
            let locationInView:CGPoint = CCDirector.sharedDirector().convertTouchToGL(touch)
            let center:CGFloat = (self.screenSize.width/2)
            
            if (locationInView.x > center){
                self.picaPau.runAction(CCActionMoveBy(duration: 0.3, position: CGPointMake(75, 0)))
            } else {
                self.picaPau.runAction(CCActionMoveBy(duration: 0.3, position: CGPointMake(-75, 0)))
            }
        } else {
            //StateMachine.sharedInstance.changeScene(StateMachineScenes.HomeScene, isFade:true)
        }
    }

    override func touchMoved(touch: UITouch!, withEvent event: UIEvent!) {
        if (self.canPlay && self.isTouching) {
            let locationInView:CGPoint = CCDirector.sharedDirector().convertTouchToGL(touch)
            self.picaPau.position = locationInView
        }
    }
    
    override func touchEnded(touch: UITouch!, withEvent event: UIEvent!) {
        self.isTouching = false
    }
    
    override func touchCancelled(touch: UITouch!, withEvent event: UIEvent!) {
        self.isTouching = false
    }
    
	// MARK: - Death Cycle
	override func onExit() {
		// Chamado quando sai do director
		super.onExit()
        self.motionManager.stopAccelerometerUpdates()
        self.motionManager.accelerometerActive == false
	}
}
