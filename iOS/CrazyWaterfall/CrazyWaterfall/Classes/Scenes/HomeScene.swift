//
//  HomeScene.swift
//  CocosSwift
//
//  Created by Usuário Convidado on 18/03/15.
//  Copyright (c) 2014 Flameworks. All rights reserved.
//
import Foundation

// MARK: - Class Definition
class HomeScene : CCScene {
	// MARK: - Public Objects

	// MARK: - Private Objects
	private let screenSize:CGSize = CCDirector.sharedDirector().viewSize()

	// MARK: - Life Cycle
	override init() {
		super.init()

        //OALSimpleAudio.sharedInstance().playBg("background.mp3")
        
        let bg1:CCSprite = CCSprite(imageNamed: "bg1.png")
        bg1.position = CGPointMake(0, 0)
        bg1.anchorPoint = CGPointMake(0, 0)
        bg1.scale = 0.5
        self.addChild(bg1)

        let bg2:CCSprite = CCSprite(imageNamed: "bg2.png")
        bg2.position = CGPointMake(0, 0)
        bg2.anchorPoint = CGPointMake(0, 0)
        bg2.scale = 0.5
        self.addChild(bg2)
        
        let bg3:CCSprite = CCSprite(imageNamed: "bg3.png")
        bg3.position = CGPointMake(0, 0)
        bg3.anchorPoint = CGPointMake(0, 0)
        bg3.scale = 0.5
        self.addChild(bg3)
        
        let logo:CCSprite = CCSprite(imageNamed: "logo.png")
        logo.position = CGPointMake(self.screenSize.width/2, self.screenSize.height/2 + 300)
        logo.anchorPoint = CGPointMake(0.5, 0.5)
        logo.scale = 0.7
        self.addChild(logo)

		// ToGame Button
        var startButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("play.png").spriteFrame)
        startButton.block = {(sender:AnyObject!) -> Void in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.GameScene, isFade:true)
            
        }
        startButton.position = CGPointMake(self.screenSize.width/2, self.screenSize.height/2)
        startButton.anchorPoint = CGPointMake(0.5, 0.5)
        startButton.scale = 0.5
        self.addChild(startButton)
        
        // Config Button
        var configButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("configs.png").spriteFrame)
        configButton.block = {(sender:AnyObject!) -> Void in
            StateMachine.sharedInstance.changeScene(StateMachineScenes.ConfigurationScene, isFade:true)
            
        }
        configButton.position = CGPointMake(startButton.positionInPoints.x, startButton.positionInPoints.y - 80)
        configButton.anchorPoint = CGPointMake(0.5, 0.5)
        configButton.scale = 0.2
        self.addChild(configButton)
      
        let highScore:CCSprite = CCSprite(imageNamed: "hiscore.png")
        highScore.position = CGPointMake(configButton.positionInPoints.x, configButton.positionInPoints.y - 150)
        highScore.anchorPoint = CGPointMake(0.5, 0.5)
        highScore.scale = 0.2
        self.addChild(highScore)
        
        let hiScore:String = "\(PropertyHelper.sharedInstance.getHiScore())"
        let labelnumeros:CCLabelTTF = CCLabelTTF(string: hiScore, fontName: "Symbol", fontSize: 32.0)
        labelnumeros.color = CCColor.whiteColor()
        labelnumeros.position = CGPointMake(highScore.positionInPoints.x, highScore.positionInPoints.y - 50)
        labelnumeros.anchorPoint = CGPointMake(0.5, 0.5)
        self.addChild(labelnumeros)
	}

	override func onEnter() {
		// Chamado apos o init quando entra no director
		super.onEnter()
	}

	override func onExit() {
		// Chamado quando sai do director
		super.onExit()
	}
}
