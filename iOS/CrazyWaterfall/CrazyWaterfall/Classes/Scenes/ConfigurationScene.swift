//
//  ConfigurationScene.swift
//  CocosSwift
//
//  Created by Usuário Convidado on 18/03/15.
//  Copyright (c) 2015 Flameworks. All rights reserved.
//

import Foundation

// MARK: - Class Definition
class ConfigurationScene : CCScene {
    // MARK: - Public Objects
    
    // MARK: - Private Objects
    private let screenSize:CGSize = CCDirector.sharedDirector().viewSize()
    
    // Sound ON/Off button
    let soundONOff:CCButton = CCButton(title: "ON", fontName: "Skia", fontSize: 36.0)
    
    // Music ON button
    let musicONOff:CCButton = CCButton(title: "ON", fontName: "Skia", fontSize: 36.0)
    
    // MARK: - Life Cycle
    override init() {
        super.init()
        
        let logo:CCSprite = CCSprite(imageNamed: "configs.png")
        logo.position = CGPointMake(self.screenSize.width/2, (self.screenSize.height / 1.1) + 50)
        logo.anchorPoint = CGPointMake(0.5, 0.5)
        //logo.scale = 0.5
        self.addChild(logo)
        
        let bg1:CCSprite = CCSprite(imageNamed: "bg1.png")
        bg1.position = CGPointMake(0, 0)
        bg1.anchorPoint = CGPointMake(0, 0)
        bg1.scale = 0.5
        self.addChild(bg1, z:0)
        
        let bg2:CCSprite = CCSprite(imageNamed: "bg2.png")
        bg2.position = CGPointMake(0, 0)
        bg2.anchorPoint = CGPointMake(0, 0)
        bg2.scale = 0.5
        self.addChild(bg2, z:0)
        
        let bg3:CCSprite = CCSprite(imageNamed: "bg3.png")
        bg3.position = CGPointMake(0, 0)
        bg3.anchorPoint = CGPointMake(0, 0)
        bg3.scale = 0.5
        self.addChild(bg3, z:0)
        
        if(SoundPlayHelper.sharedInstance.getStatusBgMusic()){
            self.musicONOff.title = "[ON]"
        } else {
            self.musicONOff.title = "[OFF]"
        }
        
        if(SoundPlayHelper.sharedInstance.getStatusFxSounds()){
            self.soundONOff.title = "[ON]"
        } else {
            self.soundONOff.title = "[OFF]"
        }
        
        // Back button
        let backButton:CCButton = CCButton(title: "", spriteFrame:CCSprite.spriteWithImageNamed("close.png").spriteFrame)
        backButton.position = CGPointMake(self.screenSize.width - 30, self.screenSize.height - 30)
        backButton.anchorPoint = CGPointMake(1.0, 1.0)
        backButton.zoomWhenHighlighted = false
        backButton.scale = 0.2
        backButton.block = {_ in StateMachine.sharedInstance.changeScene(StateMachineScenes.HomeScene, isFade:true)}
        self.addChild(backButton)
        
        // Label Sound
        let lblSound:CCSprite = CCSprite(imageNamed: "sound_efects.png")
        lblSound.position = CGPointMake(self.screenSize.width/2.5, self.screenSize.height/1.2)
        lblSound.anchorPoint = CGPointMake(0.5, 0.5)
        lblSound.scale = 0.3
        self.addChild(lblSound)
        
        // Sound ON button
        self.soundONOff.position = CGPointMake(lblSound.position.x + 200, lblSound.position.y)
        self.soundONOff.anchorPoint = CGPointMake(0.5, 0.5)
        self.soundONOff.zoomWhenHighlighted = true
        self.soundONOff.block = {_ in self.configureEffects()}
        self.addChild(soundONOff)

        // Label Music
        let labelMusic:CCSprite = CCSprite(imageNamed: "music.png")
        labelMusic.position = CGPointMake(lblSound.position.x, lblSound.position.y - 100)
        labelMusic.anchorPoint = CGPointMake(0.5, 0.5)
        labelMusic.scale = 0.2
        self.addChild(labelMusic)
        
        // Music ON button
        musicONOff.position = CGPointMake(soundONOff.position.x, labelMusic.position.y)
        musicONOff.anchorPoint = CGPointMake(0.5, 0.5)
        musicONOff.zoomWhenHighlighted = true
        musicONOff.block = {_ in self.configureMusic()}
        self.addChild(musicONOff)


        let labelCreditos:CCLabelTTF = CCLabelTTF(string: "Creditos", fontName: "Optima", fontSize: 36.0)
        labelCreditos.color = CCColor.redColor()
        labelCreditos.position = CGPointMake(self.screenSize.width / 2, musicONOff.position.y - 200)
        labelCreditos.anchorPoint = CGPointMake(0.5, 0.5)
        self.addChild(labelCreditos)
        
        let labelNomes:CCLabelTTF = CCLabelTTF(string: "46475 - Caio Waquil \n46795 - Daniel Jordão \n47149 - Davis Amaral \n46809 - Leandro Pereira \n47126 - Mauricio Frasson \n46754 - Mozart Falcão", fontName: "Optima", fontSize: 36.0)
        labelNomes.color = CCColor.whiteColor()
        labelNomes.position = CGPointMake(labelCreditos.position.x, labelCreditos.position.y - 200)
        labelNomes.anchorPoint = CGPointMake(0.5, 0.5)
        self.addChild(labelNomes)
        
    }
    
    override func onEnter() {
        // Chamado apos o init quando entra no director
        super.onEnter()
    }
    
    // Tick baseado no FPS
    override func update(delta: CCTime) {
        //...
    }
    
    // MARK: - Private Methods
    
    // MARK: - Public Methods
    
    func configureMusic(){
        if(SoundPlayHelper.sharedInstance.getStatusBgMusic()){
            self.musicONOff.title = "[OFF]"
        } else {
            self.musicONOff.title = "[ON]"
        }
        SoundPlayHelper.sharedInstance.setStatusBgMusic()
    }
    
    func configureEffects(){
        if(SoundPlayHelper.sharedInstance.getStatusFxSounds()){
            self.soundONOff.title = "[OFF]"
        } else {
            self.soundONOff.title = "[ON]"
        }
        SoundPlayHelper.sharedInstance.setStatusFxSounds()
    }
    
    // MARK: - Delegates/Datasources
    override func touchBegan(touch: UITouch!, withEvent event: UIEvent!) {

    }
    // MARK: - Death Cycle
    override func onExit() {
        // Chamado quando sai do director
        super.onExit()
    }
}