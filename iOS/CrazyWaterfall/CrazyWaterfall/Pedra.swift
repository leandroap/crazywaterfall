//
//  Pedras.swift
//  CrazyWaterfall
//
//  Created by Mozart Falcão on 22/03/15.
//  Copyright (c) 2015 Flameworks. All rights reserved.

// MARK: - Class Definition
class Pedra : CCSprite {
    // MARK: - Public Objects
    var damage:CGFloat = 12.0
    var life:CGFloat = 1.0
    var gameSceneRef:GameScene?
    
    // MARK: - Private Objects
    var numShots:Int = 5
    var shootDelayCount:Int = 0
    
    var actualStonelWaterFrame:Int = 0;
    var stoneWater:CCSprite = CCSprite(imageNamed: "stone1.png");
    
    // MARK: - Life Cycle
    override init() {
        super.init()
        
        // Configuracoes default
        self.physicsBody = CCPhysicsBody(rect: CGRectMake(0, 0, self.contentSize.width, self.contentSize.height), cornerRadius: 0.0)
        self.physicsBody.type = CCPhysicsBodyType.Kinematic
        self.physicsBody.friction = 1.0
        self.physicsBody.elasticity = 0.1
        self.physicsBody.mass = 100.0
        self.physicsBody.density = 100.0
        self.physicsBody.collisionType = "Pedras"
        self.physicsBody.collisionCategories = ["Pedras"]
        self.physicsBody.collisionMask = ["PicaPau"]
        
        self.addChild(stoneWater)
    }
    
    override init(CGImage image: CGImage!, key: String!) {
        super.init(CGImage: image, key: key)
    }
    
    override init(spriteFrame: CCSpriteFrame!) {
        super.init(spriteFrame: spriteFrame)
    }
    
    override init(texture: CCTexture!) {
        super.init(texture: texture)
    }
    
    override init(texture: CCTexture!, rect: CGRect) {
        super.init(texture: texture, rect: rect)
    }
    
    override init(texture: CCTexture!, rect: CGRect, rotated: Bool) {
        super.init(texture: texture, rect: rect, rotated: rotated)
    }
    
    override init(imageNamed imageName: String!) {
        super.init(imageNamed: imageName)
        
        self.life += CGFloat(arc4random_uniform(4)) // Ganha vidas extras de 0 a 3
        // Configuracoes default
        self.physicsBody = CCPhysicsBody(rect: CGRectMake(0, 0, self.contentSize.width, self.contentSize.height), cornerRadius: 0.0)
        self.physicsBody.type = CCPhysicsBodyType.Kinematic
        self.physicsBody.friction = 1.0
        self.physicsBody.elasticity = 0.1
        self.physicsBody.mass = 100.0
        self.physicsBody.density = 100.0
        self.physicsBody.collisionType = "Pedras"
        self.physicsBody.collisionCategories = ["Pedras"]
        self.physicsBody.collisionMask = ["PicaPau"]
    }
    
    override func onEnter() {
        // Chamado apos o init quando entra no director
        super.onEnter()
    }
    
    // Cada nave tem 40% de atirar a cada tick ateh um limite de 5x
    override func update(delta: CCTime) {
        
        var frame:NSString
        
        switch(self.actualStonelWaterFrame) {
        case 1: frame="stone1.png"; break;
        case 2: frame="stone2.png"; break;
        case 3: frame="stone3.png"; break;
        default:frame="stone1.png"; break;
        }
        
        stoneWater.spriteFrame = CCSprite(imageNamed: frame).spriteFrame;
        stoneWater.scale = 0.5
        self.actualStonelWaterFrame++;
        if(self.actualStonelWaterFrame > 3){
            self.actualStonelWaterFrame = 0;
        }
        
    }
    
    // MARK: - Private Methods
    
    // MARK: - Public Methods
    
    // MARK: - Delegates/Datasources
    
    // MARK: - Death Cycle
    deinit {
        // Chamado no momento de desalocacao
    }
}

