//
//  Carinha.swift
//  CrazyWaterfall
//
//  Created by Mozart Falcão on 21/03/15.
//  Copyright (c) 2015 Flameworks. All rights reserved.
//

class PicaPau : CCSprite {
    // MARK: - Public Objects
    var atackSpeed:CGFloat = 20.0
    var life:CGFloat = 100.0
    var canColide:Bool = true
    var actualBarrelWaterFrame:Int = 0;
    var barrelWater:CCSprite = CCSprite(imageNamed: "barrel_water_1.png");
    
    // MARK: - Private Objects
    
    // MARK: - Life Cycle
    override init() {
        super.init()
        
        
    }
    
    override init(CGImage image: CGImage!, key: String!) {
        super.init(CGImage: image, key: key)
    }
    
    override init(spriteFrame: CCSpriteFrame!) {
        super.init(spriteFrame: spriteFrame)
    }
    
    override init(texture: CCTexture!) {
        super.init(texture: texture)
    }
    
    override init(texture: CCTexture!, rect: CGRect) {
        super.init(texture: texture, rect: rect)
    }
    
    override init(texture: CCTexture!, rect: CGRect, rotated: Bool) {
        super.init(texture: texture, rect: rect, rotated: rotated)
    }
    
    override init(imageNamed imageName: String!) {
        super.init(imageNamed: imageName)
        
        self.physicsBody = CCPhysicsBody(rect: CGRectMake(0, 0, self.contentSize.width, self.contentSize.height), cornerRadius: 0.0)
        self.physicsBody.type = CCPhysicsBodyType.Kinematic
        self.physicsBody.friction = 1.0
        self.physicsBody.elasticity = 0.1
        self.physicsBody.mass = 100.0
        self.physicsBody.density = 100.0
        self.physicsBody.collisionType = "PicaPau"
        self.physicsBody.collisionCategories = ["PicaPau"]
        self.physicsBody.collisionMask = ["Pedras", "borda"]
        
        barrelWater.position = CGPoint(x: 20.0, y: 0)
        barrelWater.anchorPoint = CGPoint(x: 0, y: 0)
        barrelWater.scale = 0.5;
        self.addChild(barrelWater)
        
        
    }
    
    override func update(delta: CCTime) {
        
        var frame:NSString
        
        switch(self.actualBarrelWaterFrame) {
        case 1: frame="barrel_water_1.png"; break;
        case 2: frame="barrel_water_2.png"; break;
        case 3: frame="barrel_water_3.png"; break;
        default:frame="barrel_water_1.png"; break;
        }
        
        barrelWater.spriteFrame = CCSprite(imageNamed: frame as String).spriteFrame;
        barrelWater.scale = 0.5
        self.actualBarrelWaterFrame++;
        if(self.actualBarrelWaterFrame > 3){
            self.actualBarrelWaterFrame = 0;
        }
        
    }
    
    
    
    override func onEnter() {
        // Chamado apos o init quando entra no director
        super.onEnter()
    }
    
    // MARK: - Private Methods
    
    // MARK: - Public Methods
    
    // MARK: - Delegates/Datasources
    
    // MARK: - Death Cycle
    deinit {
        // Chamado no momento de desalocacao
    }
}
