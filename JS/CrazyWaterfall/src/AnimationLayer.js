var AnimationLayer = cc.Layer.extend({
	barrelToLeft: true,
	accelMotionControl: 0.0,
	accelMotionRange: 4.0,
	barrel: null,
	stoneVariation: 100,
	actualBarrelWaterFrame: 1,
	ctor:function (space) {
		this._super();
		this.init();
	},
	init:function () {
		//add a barrel image
		this.barrel = new cc.Sprite(res.Barrel_png);
		this.barrel.scale = 0.5;
		this.barrel.anchorY = 0;
		this.barrel.setPosition(CrazyGlobals.centerTopPos);	
		this.addChild(this.barrel, 0); 
		this.barrel.runAction( 
			cc.spawn(
				cc.scaleTo(0.95, 0.23),
				cc.moveBy(1, cc.p(0, -150))
			)
		);	
		
		this.barrelWater = new cc.Sprite(res.BarrelWater1_png);
		this.barrelWater.scale = 0.5;
		this.barrelWater.anchorY = 0;
		this.barrelWater.setPosition(CrazyGlobals.centerTopPos);	
		this.addChild(this.barrelWater, 0); 
		this.barrelWater.runAction( 
			cc.spawn(
					cc.scaleTo(0.95, 0.135),
					cc.moveBy(1, cc.p(0, -150))
			)
		);	

		if( 'accelerometer' in cc.sys.capabilities ) {
			cc.inputManager.setAccelerometerInterval(1/30);
			cc.inputManager.setAccelerometerEnabled(true);
			cc.eventManager.addListener({
				event: cc.EventListener.ACCELERATION,
				callback: this.onAccelerometer
			}, this);			
		}
		
		this.generateStone();
		this.scheduleUpdate();
	},
	onAccelerometer:function(accelEvent, event) {
		var target = event.getCurrentTarget();
		var w = CrazyGlobals.width;
		var x = target.barrel.getPositionX();
		x = x + ( accelEvent.x * w * 0.08);
		if(CrazyMachine.isGameRunning()){
			if(x >= 40 && x < w - 40){
				if(target.barrel.getPositionX() < x){
					if(!target.barrelToLeft){
						target.accelMotionControl += target.calcDiff(x, target.barrel.getPositionX());
						if(target.accelMotionControl >= target.accelMotionRange){
							target.barrel.setFlippedX(false);
							target.barrelWater.setFlippedX(false);
						}
					}else{
						target.barrelToLeft = false;
						target.accelMotionControl = 0.0;
					}					
				}else{
					if(target.barrelToLeft){
						target.accelMotionControl += target.calcDiff(x, target.barrel.getPositionX());
						if(target.accelMotionControl >= target.accelMotionRange){
							target.barrel.setFlippedX(true);
							target.barrelWater.setFlippedX(true);
						}
					}else{
						target.barrelToLeft = true;
						target.accelMotionControl = 0.0;						
					}					
				}
				target.barrel.setPositionX(x);
				target.barrelWater.setPositionX(x);
			}	
		}
	},
	update: function(dt){
		if(CrazyMachine.isGameRunning()){
			if(CrazyMachine.stoneSpeedy <= CrazyMachine.maxStoneSpeedy){
				CrazyMachine.incrementStoneSpeedy();
			}
			this.moveWater();
			this.updateStonesPosition();
		}					
	},
	calcDiff: function(num1, num2){
		return (num1 > num2)? num1-num2 : num2-num1
	},
	generateStone: function(){
		var colision = new cc.LayerColor(cc.color(255,255,255,1), 58, 17);
		var stone = new cc.Sprite(res.Stone1_png);
		stone.scale = 0.3;
		stone.setPositionY(0);	
		var xPosition = Math.floor((Math.random() * (CrazyGlobals.width - 100)) + 70);
		stone.setPositionX(xPosition);
		colision.setPositionX(xPosition-24);
		this.addChild(stone, 5, ""+this.random(150, 300));
		this.addChild(colision, 6);
		CrazyMachine.colisions.push(colision);
		CrazyMachine.stones.push(stone);		
	},
	updateStonesPosition: function(){	
		for(var i = 0; i < CrazyMachine.stones.length; i++){ 
			var stone = CrazyMachine.stones[i];
			var colision = CrazyMachine.colisions[i];
			stone.setPositionY(stone.getPositionY() + CrazyMachine.stoneSpeedy);
			colision.setPositionY(colision.getPositionY() + CrazyMachine.stoneSpeedy);
			if(stone.getPositionY() > CrazyGlobals.height + 50){
				colision.removeFromParent(true); 
				stone.removeFromParent(true); 
				
				CrazyMachine.stones.splice(i, 1);
				CrazyMachine.colisions.splice(i, 1);
				
				CrazyMachine.incrementScore(); 
				continue;			
			}
			if(stone.getPositionY() > parseInt(stone.getTag())){
				stone.setTag(CrazyGlobals.height + 250)
				this.generateStone();  
			}
			var barrelRect = this.barrel.getBoundingBox();
			var stoneRect = colision.getBoundingBox();
			if(cc.rectIntersectsRect(barrelRect, stoneRect)){
				this.gameOver();
			}
		}				
	},
	gameOver: function(){		
		CrazyMachine.gameOver();
		this.addChild(new GameOverLayer(), 10);
	},
	random: function(min,max){
		return Math.floor(Math.random()*(max-min+1)+min);
	},
	moveWater: function(){
		var barrelFrame = this.actualBarrelWaterFrame==1?res.BarrelWater1_png:this.actualBarrelWaterFrame==2?res.BarrelWater2_png:res.BarrelWater3_png;
		var stoneFrame = this.actualBarrelWaterFrame==1?res.Stone1_png:this.actualBarrelWaterFrame==2?res.Stone2_png:res.Stone3_png;
		this.barrelWater.setTexture(barrelFrame);	
		for(var i = 0; i < CrazyMachine.stones.length; i++){ 
			var stone = CrazyMachine.stones[i];
			stone.setTexture(stoneFrame);	
		}
		this.actualBarrelWaterFrame ++;
		if(this.actualBarrelWaterFrame>3){
			this.actualBarrelWaterFrame = 0;
		}
	}
});