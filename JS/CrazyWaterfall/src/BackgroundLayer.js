var BackgroundLayer = cc.Layer.extend({
	bg1a: null,	
	bg1b: null,
	bg2a: null,
	bg2b: null,
	bg3a: null,
	bg3b: null,
	ctor:function (space) {
		this._super();
		this.init();
	},
	init:function () {
		//add a blue bg layer
		this.bg0 = new cc.LayerColor(cc.color(41,170,227,255), CrazyGlobals.width, CrazyGlobals.height);
		//add first parallax layer
		this.bg1a = new cc.Sprite(res.Bg1_png);
		this.bg1a.scale = 0.20;
		this.bg1a.anchorY = 0;
		this.bg1a.setPosition(CrazyGlobals.centerBottomPos);
		
		this.bg1b = new cc.Sprite(res.Bg1_png);
		this.bg1b.scale = 0.20;
		this.bg1b.anchorY = 0;
		this.bg1b.setPosition(CrazyGlobals.centerTopPos);  
		
		//add second parallax layer
		this.bg2a = new cc.Sprite(res.Bg2_png);
		this.bg2a.scale = 0.20;
		this.bg2a.anchorY = 0;
		this.bg2a.setPosition(CrazyGlobals.centerBottomPos);  

		this.bg2b = new cc.Sprite(res.Bg2_png);
		this.bg2b.scale = 0.20;
		this.bg2b.anchorY = 0;
		this.bg2b.setPosition(CrazyGlobals.centerTopPos);  
		
		//add side stones layer
		this.bg3a = new cc.Sprite(res.Bg3_png);
		this.bg3a.scale = 0.23;
		this.bg3a.anchorY = 1;
		this.bg3a.setPosition(CrazyGlobals.centerBottomPos);  

		this.bg3b = new cc.Sprite(res.Bg3_png);
		this.bg3b.scale = 0.235;
		this.bg3b.anchorY = 1;
		this.bg3b.setPosition(CrazyGlobals.centerTopPos); 
		
		//put all on layer
		this.addChild(this.bg0, 0);
		this.addChild(this.bg1a, 1);
		this.addChild(this.bg1b, 2);
		this.addChild(this.bg2a, 5);
		this.addChild(this.bg2b, 3);
		this.addChild(this.bg3a, 6);
		this.addChild(this.bg3b, 4);
		
		this.scheduleUpdate();
		
	},
	update : function(dt)
	{
		
		this.bg1a.setPositionY(this.getNewWaterPosition(this.bg1a,4)); 
		this.bg1b.setPositionY(this.getNewWaterPosition(this.bg1b,4)); 
		this.bg2a.setPositionY(this.getNewWaterPosition(this.bg2a,6)); 
		this.bg2b.setPositionY(this.getNewWaterPosition(this.bg2b,6));
		if(CrazyMachine.isGameRunning()){
			this.bg3a.setPositionY(this.getNewStonePosition(this.bg3a,4)); 
			this.bg3b.setPositionY(this.getNewStonePosition(this.bg3b));			
		}
		
	},
	getNewWaterPosition: function(element, speed){
		var value = element.getPositionY() - speed;    	
		if(element.getPositionY() < - CrazyGlobals.height){
			value = CrazyGlobals.height;
		}
		return value;
	},
	getNewStonePosition: function(element){
		var value = element.getPositionY() + CrazyMachine.stoneSpeedy;    	
		if(element.getPositionY() >  CrazyGlobals.height * 2 ){
			value = 0;
		}
		return value;
	}
});