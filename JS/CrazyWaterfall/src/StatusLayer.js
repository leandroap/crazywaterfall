var StatusLayer = cc.Layer.extend({
	scoreLabel: null,
	ctor:function () {
		this._super();
		this.init();
	},
	init: function(){
		//create pause manu
		var menuItemPause= new cc.MenuItemSprite(
				new cc.Sprite(res.Pause_png), 
				new cc.Sprite(res.Pause_png),
				this.pause, this);
		menuItemPause.scale = 0.12;
		//add pause image
		var pause = new cc.Menu(menuItemPause);     
		pause.setPosition(CrazyGlobals.leftTopPos);
		pause.setPositionY(pause.getPositionY()-20);
		pause.setPositionX(pause.getPositionX()+20);
		this.addChild(pause);		
		
//		add score image
		var score = new cc.Sprite(res.Score_png);
		score.scale = 0.12;
		score.setPosition(CrazyGlobals.rightTopPos);
		score.setPositionY(score.getPositionY()-20);
		score.setPositionX(score.getPositionX()-120);
		this.addChild(score); 
		
		this.scoreLabel = new cc.LabelTTF("0",  "Arial", 27, cc.size(100,30), cc.TEXT_ALIGNMENT_CENTER);
		//this.scoreLabel.setFontFillColor(new cc.Color(255, 255, 255, 255));
		this.scoreLabel.setPosition(CrazyGlobals.rightTopPos);
		this.scoreLabel.setPositionY(this.scoreLabel.getPositionY()-17);
		this.scoreLabel.setPositionX(this.scoreLabel.getPositionX()-63);
		this.addChild(this.scoreLabel);
		this.scheduleUpdate();
	},
	update: function(){
		this.scoreLabel.setString(CrazyMachine.currentScore);
	},
	setScoreLabel: function(text) {
		this.scoreLabel.setString(text);
	},
	pause: function(){
		if(!CrazyMachine.isGamePaused()){
			this.addChild(new PauseLayer(), 10);
			CrazyMachine.pauseGame();			
		}
	}
});