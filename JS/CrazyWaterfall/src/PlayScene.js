var PlayScene = cc.Scene.extend({
	onEnter:function () {
        this._super();        
        //add background layer (waterfall)
        var bgLayer = new BackgroundLayer();
        this.addChild(bgLayer, 0);        
        //add animation layer (barrel)
        var animationLayer = new AnimationLayer();
        this.addChild(animationLayer, 1); 
        //add status layer (score)
        var statusLayer = new StatusLayer();
        this.addChild(statusLayer, 2);        
	}
});