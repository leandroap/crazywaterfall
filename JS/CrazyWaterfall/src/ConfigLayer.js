var ConfigLayer = cc.Layer.extend({
	soundCheckItem: null,
	musicCheckItem: null,
	ctor:function () {
		this._super();
		this.init();
	},
	init: function(){
		var menuBg = new cc.Sprite(res.Bg1_png);
		menuBg.scale = 0.25;
		menuBg.setPosition(CrazyGlobals.centerCenterPos);
		this.addChild(menuBg, 0);
		
		//add score image
		var options = new cc.Sprite(res.Configs_png);
		options.scale = 0.18;
		options.setPosition(CrazyGlobals.centerTopPos);
		options.setPositionY(options.getPositionY()-50);
		this.addChild(options);
		
		//add score image
		var sounds = new cc.Sprite(res.SoudEffects_png);
		sounds.scale = 0.21;
		sounds.setPosition(CrazyGlobals.centerCenterPos);
		sounds.setPositionY(sounds.getPositionY()+100);
		this.addChild(sounds); 
		
		this.soundCheckItem = new cc.MenuItemToggle( 
				new cc.MenuItemFont("On"), 
				new cc.MenuItemFont("Off"), 
				this.changeEffects, this)
		
		var soundIndex = CrazyMachine.effectsOn ? 0 : 1;
		this.soundCheckItem.setSelectedIndex(soundIndex);
		var soundCheck =  new cc.Menu(this.soundCheckItem);  
		soundCheck.setPosition(CrazyGlobals.centerCenterPos);
		soundCheck.setPositionY(sounds.getPositionY()-60);
		this.addChild(soundCheck);
		
		
		//add score image
		var music = new cc.Sprite(res.Music_png);
		music.scale = 0.13;
		music.setPosition(CrazyGlobals.centerCenterPos);
		music.setPositionY(music.getPositionY()-20);
		this.addChild(music);		
		
		this.musicCheckItem = new cc.MenuItemToggle( 
				new cc.MenuItemFont("On"), 
				new cc.MenuItemFont("Off"), 
				this.changeMusics, this)
		
		var musicIndex = CrazyMachine.musicOn ? 0 : 1;
		this.musicCheckItem.setSelectedIndex(musicIndex);
		var musicCheck =  new cc.Menu(this.musicCheckItem);  
		musicCheck.setPosition(CrazyGlobals.centerCenterPos);
		musicCheck.setPositionY(sounds.getPositionY()-160);
		this.addChild(musicCheck);
					
		//create play manu
		var menuItemSave= new cc.MenuItemSprite(
				new cc.Sprite(res.Exit_png), 
				new cc.Sprite(res.ExitPessed_png),
				this.onExit, this);
		menuItemSave.scale = 0.18;
		//add play image
		var save = new cc.Menu(menuItemSave);     
		save.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(save);		
		save.runAction(
			cc.spawn(
					cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-160))
			)
		);
		//create configMenu menu
		var configMenuItem = new cc.MenuItemSprite(
				new cc.Sprite(res.Config_png), 
				new cc.Sprite(res.ConfigPressed_png),
				this.onConfig, this);
		configMenuItem.scale = 0.14;
		//add configMenu image
		var configMenu = new cc.Menu(configMenuItem);     
		configMenu.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(configMenu);
		//run play animation
		configMenu.runAction(
			cc.spawn(
					cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-100))
			)
		);
	},
	changeEffects: function(){
		CrazyMachine.setEffects();
	},
	changeMusics: function(){
		CrazyMachine.setMusics();
	},
	onExit: function(){
		CrazyMachine.openMenu();
	}
});