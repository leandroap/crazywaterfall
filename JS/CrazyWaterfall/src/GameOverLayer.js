var GameOverLayer = cc.Layer.extend({
	ctor:function () {
		this._super();
		this.init();
	},
	init: function(){
		var bg = new cc.LayerColor(cc.color(0,0,0,180), CrazyGlobals.width, CrazyGlobals.height);
		this.addChild(bg , 0);
		
		var gameOver = new cc.Sprite(res.GameOver_png);
		gameOver.scale = 0.0;
		gameOver.setPosition(CrazyGlobals.centerCenterPos);
		gameOver.setPositionY(gameOver.getPositionY()+90);
		this.addChild(gameOver);		
		//run logo animation
		gameOver.runAction(
				cc.spawn(
						cc.scaleTo(0.7, 0.35)
				)
		);	
		
		//add score image
		var hiScore = new cc.Sprite(res.HiScore_png);
		hiScore.scale = 0.11;
		hiScore.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(hiScore); 
		hiScore.runAction(
				cc.spawn(
						cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-20))
				)
		);

		var hiScoreLabel = new cc.LabelTTF(CrazyMachine.hiScore,  "Arial", 30, cc.size(100,32), cc.TEXT_ALIGNMENT_CENTER);
		hiScoreLabel.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(hiScoreLabel); 
		hiScoreLabel.runAction(
			cc.spawn(
					cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-50))
			)
		);
		
		
		//create play manu
		var menuItemReplay= new cc.MenuItemSprite(
				new cc.Sprite(res.Replay_png), 
				new cc.Sprite(res.ReplayPressed_png),
				this.replay, this);
		menuItemReplay.scale = 0.15;
		//add play image
		var replay = new cc.Menu(menuItemReplay);     
		replay.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(replay);
		replay.runAction(
				cc.spawn(
						cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-120))
				)
		);
		//create configMenu menu
		var exitMenuItem = new cc.MenuItemSprite(
				new cc.Sprite(res.Exit_png), 
				new cc.Sprite(res.ExitPessed_png),
				this.exit, this);
		exitMenuItem.scale = 0.11;
		//add configMenu image
		var exit = new cc.Menu(exitMenuItem);     
		exit.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(exit);
		//run play animation
		exit.runAction(
				cc.spawn(
						cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-200))
				)
		);
	},
	replay: function(){
		CrazyMachine.playGame();
	},
	exit: function(){
		CrazyMachine.openMenu();
	}
});