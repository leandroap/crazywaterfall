var CrazyGlobals = {
	winSize: null,
	width: null,
	height: null,
	midleWidth: null,
	midleHeight: null,
	centerCenterPos: null,
	centerTopPos: null,
	centerBottomPos: null,
	leftCenterPos: null,
	leftTopPos: null,
	leftBottomPos: null,
	rightCenterPos: null,
	rightTopPos: null,
	rightBottomPos: null,
	init: function(){
		this.winSize = cc.director.getWinSize();
		this.width = this.winSize.width;
		this.height = this.winSize.height;
		this.midleWidth = this.width/2;
		this.midleHeight = this.height/2;

		this.centerCenterPos = cc.p(this.midleWidth, this.midleHeight);
		this.centerTopPos = cc.p(this.midleWidth, this.height);
		this.centerBottomPos = cc.p(this.midleWidth, 0.0);

		this.leftCenterPos = cc.p(0.0, this.midleHeight);
		this.leftTopPos = cc.p(0.0, this.height);
		this.leftBottomPos = cc.p(0.0, 0.0);

		this.rightCenterPos = cc.p(this.width, this.midleHeight);
		this.rightTopPos = cc.p(this.width, this.height);
		this.rightBottomPos = cc.p(this.width, 0.0);
	}
}

