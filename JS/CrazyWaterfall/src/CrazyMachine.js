var CrazyMachine = {
	hiScore: 0,
	currentScore: 0,
	stones: [],
	colisions: [],
	currentMusic: null,
	musicVolume: 0.1,
	musicOn: true,
	effectsVolume: 0.2,
	effectsOn: true,
	gameRunning: false,
	gamePaused: false,
	stoneSpeedy: 2,
	maxStoneSpeedy: 10,
	speedyAcceleration: 1.0003,
	playGame: function(){
		this.clearStones();
		cc.director.runScene(new PlayScene());
		this.gameRunning = true;
		this.gamePaused = false;		
		this.stoneSpeedy = 2;
		this.currentScore = 0;
		this.stopAllSounds();
		this.playMusic(res.GameMusic_mp3, true);
		this.playEffect(res.Waterfall_mp3, true);
		this.playEffect(res.GamePlay_mp3, false);
		
	},
	pauseGame: function(){
		this.gameRunning = false;
		this.gamePaused = true;		
	}, 
	resumeGame: function(){
		this.gameRunning = true;
		this.gamePaused = false;		
	},
	incrementScore: function(){
		if(this.isGameRunning()){
			this.currentScore++;
			if(this.currentScore>this.hiScore){
				this.hiScore = this.currentScore;
			}
			StatusLayer.setScoreLabel("cm");			
		}		
	},
	gameOver: function(){
		this.stoneSpeedy = 2;
		this.gameRunning = false;
		this.gamePaused = false;
		this.stopAllSounds();
		this.playEffect(res.Colision_mp3, false);
		this.playEffect(res.GameOver_mp3, false);		
	},
	quitGame: function(){
		this.gameRunning = false;
		this.gamePaused = false;		
	},
	openMenu: function(){
		this.gameRunning = false;
		this.gamePaused = false;		
		this.stopAllSounds();
		this.playMusic(res.GameMenuMusic_mp3, true);
		cc.director.runScene(new MenuScene());
	},
	openConfig: function(){
		this.gameRunning = false;
		this.gamePaused = false;		
		this.stopAllSounds();
		this.playMusic(res.GameMenuMusic_mp3, true);
		cc.director.runScene(new ConfigScene());
	},
	isGamePaused: function(){
		return this.gamePaused;
	},
	isGameRunning: function(){
		return this.gameRunning;
	},
	setMusics: function(){
		this.musicOn = !this.musicOn;
		if(!this.musicOn){
			if(this.currentMusic != null){
				cc.audioEngine.stopMusic(this.currentMusic);
			}
		}else if(this.currentMusic != null){
			cc.audioEngine.playMusic(this.currentMusic, true);
		}
	},
	setEffects: function(){
		this.effectsOn = !this.effectsOn;
		if(!this.effectsOn){
			cc.audioEngine.stopAllEffects();
		}
	},
	playMusic: function(music, loop){
		if(this.musicOn){
			cc.audioEngine.playMusic(music, loop)
			cc.audioEngine.setMusicVolume(this.musicVolume);
			this.currentMusic = music;
		}
	},
	playEffect: function(effect, loop){
		if(this.effectsOn){
			cc.audioEngine.playEffect(effect, loop);
			cc.audioEngine.setEffectsVolume(this.effectsVolume);
		}
	},
	stopAllSounds: function(){
		cc.audioEngine.stopAllEffects();
		if(this.currentMusic != null){
			cc.audioEngine.stopMusic(this.currentMusic);
		}
	},
	incrementStoneSpeedy: function(){
		this.stoneSpeedy *= this.speedyAcceleration;
	},
	clearStones: function(){
		if(this.stones.length>0){
			for (var i = 0; i < this.stones; i++) {
				var sto = this.stones[i];
				sto.removeFromParent(true);
				var col = this.colisions[i]
				col.removeFromParent(true);
			}
		}
		this.stones = [];
		this.colisions = [];
	}
}