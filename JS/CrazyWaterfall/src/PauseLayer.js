var PauseLayer = cc.Layer.extend({
	ctor:function () {
		this._super();
		this.init();
	},
	init: function(){
		var bg = new cc.LayerColor(cc.color(0,0,0,180), CrazyGlobals.width, CrazyGlobals.height);
		this.addChild(bg , 0);
		//create resume menu
		var menuItemResume= new cc.MenuItemSprite(
				new cc.Sprite(res.Resume_png), 
				new cc.Sprite(res.ResumePressed_png),
				this.replay, this);
		menuItemResume.scale = 0.14;
		var resume = new cc.Menu(menuItemResume);     
		resume.setPosition(CrazyGlobals.centerCenterPos);
		resume.setPositionY(resume.getPositionY()+30)
		this.addChild(resume);
		
		//create quit menu
		var exitMenuItem = new cc.MenuItemSprite(
				new cc.Sprite(res.Exit_png), 
				new cc.Sprite(res.ExitPessed_png),
				this.exit, this);
		exitMenuItem.scale = 0.14;
		//add configMenu image
		var exit = new cc.Menu(exitMenuItem);     
		exit.setPosition(CrazyGlobals.centerCenterPos);
		exit.setPositionY(exit.getPositionY()-30)
		this.addChild(exit);	
		this.scheduleUpdate();
	},
	update: function(){
		
	},
	replay: function(){
		CrazyMachine.resumeGame();
		this.removeFromParent(true);
	},
	exit: function(){
		CrazyMachine.openMenu();
	}
});