var MenuLayer = cc.Layer.extend({
	ctor:function () {
		this._super();
		this.init();
	},
	init: function(){
		//add bg sprite
		var menuBg = new cc.Sprite(res.Bg1_png);
		menuBg.scale = 0.25;		
		menuBg.setPosition(CrazyGlobals.centerCenterPos);
		this.addChild(menuBg, 0);
		
		//add logo
		var logo = new cc.Sprite(res.Logo_png);
		logo.scale = 0.0;
		logo.setPosition(CrazyGlobals.centerCenterPos);
		logo.setPositionY(logo.getPositionY()+70);
		this.addChild(logo);		
		//run logo animation
		logo.runAction(
			cc.spawn(
				cc.scaleTo(0.7, 0.35)
			)
		);		
		//create play menu
		var menuItemPlay= new cc.MenuItemSprite(
				new cc.Sprite(res.Play_png), 
				new cc.Sprite(res.PlayPressed_png),
				this.play, this);
		menuItemPlay.scale = 0.15;
		//add play image
		var play = new cc.Menu(menuItemPlay);     
		play.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(play);
		play.runAction(
			cc.spawn(
				cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-100))
			)
		);
		//create configMenu menu
		var configMenuItem = new cc.MenuItemSprite(
				new cc.Sprite(res.Configs_png), 
				new cc.Sprite(res.ConfigsPressed_png),
				this.config, this);
		configMenuItem.scale = 0.12;
		//add configMenu image
		var configMenu = new cc.Menu(configMenuItem);     
		configMenu.setPosition(CrazyGlobals.centerBottomPos);
		this.addChild(configMenu);
		//run play animation
		configMenu.runAction(
			cc.spawn(
					cc.moveBy(0.7, cc.p(0, CrazyGlobals.midleHeight-190))
			)
		);
	},
	play: function(){
		CrazyMachine.playGame();
	},
	config: function(){
		CrazyMachine.openConfig();
	}
});