cc.game.onStart = function(){
    cc.view.adjustViewPort(true);
    cc.view.setDesignResolutionSize(320, 480, cc.ResolutionPolicy.SHOW_ALL);
    cc.view.resizeWithBrowserSize(true);
    cc.eventManager.setEnabled(true);
    //load resources
    cc.LoaderScene.preload(g_resources, function () {
    	CrazyGlobals.init();    	
    	CrazyMachine.openMenu();
    }, this);
};
cc.game.run();